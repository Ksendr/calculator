//
//  Calc.m
//  calculator
//
//  Created by Admin on 10/26/14.
//  Copyright (c) 2014 123. All rights reserved.
//

#import "Calc.h"

@interface Calc(){
    NSString *contents;
}
@end

@implementation Calc
-(id)init{
    self = [super init];
    if(self){
        contents = @"";
    }
    return self;
}
-(NSString *)append:(NSString *)inStr{
    if([inStr isEqualToString:@"_"]){
        NSArray *tmp = [self split];
        if([tmp count] < 2)
        contents = [contents stringByAppendingString:@" "];
    }
    else if([inStr isEqualToString:@"."]){
        NSArray *tmp = [self split];
        if(![(NSString *)tmp[[tmp count]-1] containsString:@"."]){
            contents = [contents stringByAppendingString:@"."];
        }
    }
    else if([inStr isEqualToString:@"-"]){
        NSArray *tmp = [self split];
        if([(NSString *)tmp[[tmp count]-1] length] == 0){
            contents = [contents stringByAppendingString:@"-"];
        }
    }
    else
        contents = [contents stringByAppendingString:inStr];
    return contents;
}
-(NSString *)remove{
    if([contents length] > 0)
        contents = [contents substringToIndex:[contents length]-1];
    return contents;
}
-(NSString *)clear{
    contents = @"";
    return contents;
}
-(NSString *)add{
    NSArray *tmp = [self split];
    if([tmp count] == 2 && ![tmp[1] isEqualToString:@""]){
        contents = [NSString stringWithFormat:@"%f", [self parse:tmp[0]] + [self parse:tmp[1]]];
    }
    return contents;
}
-(NSString *)sub{
    NSArray *tmp = [self split];
    if([tmp count] == 1 || ([tmp count] ==2 && [tmp[1] isEqualToString:@""]))
        return [self append:@"-"];
    if([tmp count] == 2 && ![tmp[1] isEqualToString:@""]){
        contents = [NSString stringWithFormat:@"%f", [self parse:tmp[0]] - [self parse:tmp[1]]];
    }
    return contents;
}
-(NSString *)mul{
    NSArray *tmp = [self split];
    if([tmp count] == 2 && ![tmp[1] isEqualToString:@""]){
        contents = [NSString stringWithFormat:@"%f", [self parse:tmp[0]] * [self parse:tmp[1]]];
    }
    return contents;
}
-(NSString *)div{
    NSArray *tmp = [self split];
    if([tmp count] == 2 && ![tmp[1] isEqualToString:@""]){
        contents = [NSString stringWithFormat:@"%f", [self parse:tmp[0]] / [self parse:tmp[1]]];
    }
    return contents;
}
-(NSString *)frac{
    NSArray *tmp = [self split];
    if([tmp count] == 1 || ([tmp count] == 2 && [tmp[1] isEqualToString:@""])){
        contents = [NSString stringWithFormat:@"%f", 1 / [self parse:tmp[0]]];
    }
    return contents;
}
-(NSString *)pow{
    NSArray *tmp = [self split];
    if([tmp count] == 2 && ![tmp[1] isEqualToString:@""]){
        contents = [NSString stringWithFormat:@"%f", powf([self parse:tmp[0]], [self parse:tmp[1]])];
    }
    return contents;
}
-(NSArray *)split{
    return [contents componentsSeparatedByString:@" "];
}
-(double)parse:(NSString *)str{
    if([str isEqualToString:@""])
        return 0.0;
    double tmp = [str doubleValue];
    return tmp;
}
@end
