//
//  Calc.h
//  calculator
//
//  Created by Admin on 10/26/14.
//  Copyright (c) 2014 123. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calc : NSObject
-(id)init;
-(NSString *)append:(NSString *)inStr;
-(NSString *)remove;
-(NSString *)clear;
-(NSString *)add;
-(NSString *)sub;
-(NSString *)mul;
-(NSString *)div;
-(NSString *)frac;
-(NSString *)pow;
@end
