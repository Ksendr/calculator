//
//  ViewController.m
//  calculator
//
//  Created by Admin on 10/26/14.
//  Copyright (c) 2014 123. All rights reserved.
//

#import "ViewController.h"
#import "Calc.h"

@interface ViewController (){
    Calc *calc;
}
@property (weak, nonatomic) IBOutlet UILabel *lResult;
@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    calc = [[Calc alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pushButton:(UIButton *)sender {
    [self print:[calc append:sender.titleLabel.text]];
}
- (IBAction)pushBtnBack:(UIButton *)sender {
    [self print:[calc remove]];
}
- (IBAction)pushBtnDel:(UIButton *)sender {
    [self print:[calc clear]];
}
- (IBAction)pushBtnAdd:(UIButton *)sender {
    [self print:[calc add]];
}
- (IBAction)pushBtnSub:(UIButton *)sender {
    [self print:[calc sub]];
}
- (IBAction)pushBtnMul:(UIButton *)sender {
    [self print:[calc mul]];
}
- (IBAction)pushBtnDiv:(UIButton *)sender {
    [self print:[calc div]];
}
- (IBAction)pushBtnFrac:(UIButton *)sender {
    [self print:[calc frac]];
}
- (IBAction)pushBtnPow:(UIButton *)sender {
    [self print:[calc pow]];
}

-(void)print:(NSString *)s{
    self.lResult.text = s;
}

@end
